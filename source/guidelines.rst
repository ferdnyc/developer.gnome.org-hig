Guidelines
==========

The guidelines section covers the standard conventions used in GNOME UX design. These are all generally applicable, and are relevant to all apps and design patterns.

.. toctree::
   :hidden:

   guidelines/app-naming
   guidelines/app-icons
   guidelines/pointer-touch
   guidelines/keyboard
   guidelines/ui-icons
   guidelines/ui-styling
   guidelines/writing-style
   guidelines/typography
   guidelines/navigation
   guidelines/adaptive
   guidelines/accessibility

.. cssclass:: tiled-toc

*  .. image:: img/tiles/guidelines-appnaming.svg
      :target: guidelines/app-naming.html
      :class: only-light
   .. image:: /img/tiles/guidelines-appnaming-dark.svg
      :target: guidelines/app-naming.html
      :class: only-dark
      
   :doc:`App Naming </guidelines/app-naming>`

*  .. image:: img/tiles/guidelines-appicons.svg
      :target: guidelines/app-icons.html
      :class: only-light
   .. image:: /img/tiles/guidelines-appicons-dark.svg
      :target: guidelines/app-icons.html
      :class: only-dark
      
   :doc:`App Icons </guidelines/app-icons>` 

*  .. image:: img/tiles/guidelines-pointertouch.svg
      :target: guidelines/pointer-touch.html
      :class: only-light
   .. image:: /img/tiles/guidelines-pointertouch-dark.svg
      :target: guidelines/pointer-touch.html
      :class: only-dark
      
   :doc:`Pointer & Touch </guidelines/pointer-touch>` 

*  .. image:: img/tiles/guidelines-keyboard.svg
      :target: guidelines/keyboard.html
      :class: only-light
   .. image:: /img/tiles/guidelines-keyboard-dark.svg
      :target: guidelines/keyboard.html
      :class: only-dark
      
   :doc:`Keyboard </guidelines/keyboard>` 

*  .. image:: img/tiles/guidelines-uiicons.svg
      :target: guidelines/ui-icons.html
      :class: only-light
   .. image:: /img/tiles/guidelines-uiicons-dark.svg
      :target: guidelines/ui-icons.html
      :class: only-dark
      
   :doc:`UI Icons </guidelines/ui-icons>` 

*  .. image:: img/tiles/guidelines-uistyling.svg
      :target: guidelines/ui-styling.html
      :class: only-light
   .. image:: /img/tiles/guidelines-uistyling-dark.svg
      :target: guidelines/ui-styling.html
      :class: only-dark
      
   :doc:`UI Styling </guidelines/ui-styling>` 

*  .. image:: img/tiles/guidelines-writingstyle.svg
      :target: guidelines/writing-style.html
      :class: only-light
   .. image:: /img/tiles/guidelines-writingstyle-dark.svg
      :target: guidelines/writing-style.html
      :class: only-dark
      
   :doc:`Writing Style </guidelines/writing-style>` 

*  .. image:: img/tiles/guidelines-typography.svg
      :target: guidelines/typography.html
      :class: only-light
   .. image:: /img/tiles/guidelines-typography-dark.svg
      :target: guidelines/typography.html
      :class: only-dark
      
   :doc:`Typography </guidelines/typography>`

*  .. image:: img/tiles/guidelines-navigation.svg
      :target: guidelines/navigation.html
      :class: only-light
   .. image:: /img/tiles/guidelines-navigation-dark.svg
      :target: guidelines/navigation.html
      :class: only-dark
      
   :doc:`Navigation </guidelines/navigation>`

*  .. image:: img/tiles/guidelines-scalingandadaptiveness.svg
      :target: guidelines/adaptive.html
      :class: only-light
   .. image:: /img/tiles/guidelines-scalingandadaptiveness-dark.svg
      :target: guidelines/adaptive.html
      :class: only-dark
      
   :doc:`Scaling & Adaptiveness </guidelines/adaptive>`

*  .. image:: img/tiles/guidelines-accessibility.svg
      :target: guidelines/accessibility.html
      :class: only-light
   .. image:: /img/tiles/guidelines-accessibility-dark.svg
      :target: guidelines/accessibility.html
      :class: only-dark
      
   :doc:`Accessibility </guidelines/accessibility>` 
