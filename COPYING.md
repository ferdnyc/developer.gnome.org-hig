## Per-file attritubion

Allan Day, Calum Benson, Adam Elman, Seth Nickell, Colin Robertson:

* buttons.rst
* check-boxes.rst:
* dialogs.rst
* keyboard-interaction.rst
* menus.rst
* radio-buttons.rst
* sliders.rst
* spin-buttons.rst
* text-fields.rst
* progress-bars.rst
* tabs.rst
* model-based.rst
* drop-downs.rst

Allan Day, William Jon McCann:

* typography.rst

Allan Day, Calum Benson, Adam Elman, Seth Nickell, Colin Robertson, Ekaterina Gerasimova:

* writing-style.rst